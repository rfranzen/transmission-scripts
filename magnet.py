#!/usr/bin/env python2

import os
import subprocess
import time
from glob import glob

TRANSMISSION_PATH = '/usr/bin/transmission-gtk'
WATCHED_DIRECTORY = './'
LOG_FILE = './magnet.log'


def call(cmd):
	subprocess.Popen(cmd, shell=True, stdin=None, stderr=None, stdout=None)

while True:
	time.sleep(1)
	for filename in glob('{0}/*.magnet'.format(WATCHED_DIRECTORY)):
		with open(LOG_FILE, 'a+') as log:
			log.write('Adding file: {0}\n'.format(filename))

		with open(filename, 'r') as handle:
			lines = handle.read().strip()
			call(TRANSMISSION_PATH + ' ' + lines)

		os.unlink(filename)
